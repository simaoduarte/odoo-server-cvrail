# -*- coding: utf-8 -*-
{
    'name': "Cvrail",

    'summary': 'Slip detection on slopes',

    'description': """ 
        This work intends to detect these accidents using computer vision
    """,

    'author': "Simão Duarte",
    'website': "",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'sequence': '10',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['mail', 'base', 'board', 'project'],
    # always loaded
    'images': [],
    'data': [
        'security/ir.model.access.csv',
        'views/sequence.xml',
        'views/cvrail.xml',
        'views/status.xml'
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': True,
}