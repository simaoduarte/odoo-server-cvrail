# -*- coding: utf-8 -*-

from odoo import models, fields


class Status(models.Model):
    _name = 'cvrail.status'
    _description = 'Status cvrail'
    _rec_name = 'number_state'

    number_state = fields.Selection([
        ('New', 'New'),
        ('In progress', 'In progress'),
        ('End', 'End'),
        ('Cancelled', 'Cancelled'),
        ('Keep alive', 'Keep alive'),
    ], default='New', string="Status")

