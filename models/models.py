# -*- coding: utf-8 -*-

from odoo import models, fields, _, api, tools
from odoo.exceptions import ValidationError


class cvrail(models.Model):
    _name = 'open.cvrail'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _description = 'Local'

    @api.constrains('date_deadline')
    def date_diff(self):
        for rec in self:
            if rec.date_deadline < rec.date:
                raise ValidationError(_('You must enter a data_deadline greater than the date of receipt (Date)'))

    name = fields.Char(string="Locality", track_visibility="onchange")
    notes = fields.Text(string="Notes")
    reason = fields.Char(string="Reason")
    date_deadline = fields.Datetime(string='Deadline')
    date = fields.Datetime(string='Date', readonly=True)
    image_ids = fields.One2many('open.cvrail.image', 'image_id', string="Image")
    name_seq = fields.Char(string='ID', required=True, copy=False, readonly=True,
                           index=True, default=lambda self: _('New'))
    kanban_state = fields.Selection([('normal', 'In progress'), ('done', 'Done'), ('blocked', 'Technical problems')],
                                    copy=False, default='normal')
    name_company = fields.Many2one('res.partner', string="Company")
    status = fields.Many2one('cvrail.status', group_expand='_read_group_stage_ids', string="Status_id")

    @api.model
    def create(self, values):
        if values.get('name_seq', _('New')) == _('New'):
            values['name_seq'] = self.env['ir.sequence'].next_by_code('open.cvrail.sequence') or _('New')
        result = super(cvrail, self).create(values)
        return result

    @api.model
    def _read_group_stage_ids(self, stages, domain, order):
        stage_ids = self.env['cvrail.status'].search([])
        return stage_ids


class image(models.Model):
    _name = 'open.cvrail.image'
    _description = 'Image Lines'

    n_image = fields.Integer("N_Image")
    photo = fields.Binary(string="Image")
    image_id = fields.Many2one('open.cvrail', string='Image_ID')
